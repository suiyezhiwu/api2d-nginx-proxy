# api2d-nginx-proxy


## Docker 命令启动

```bash
docker run -p 9000:9000 -d easychen/api2d-proxy
````

## Docker-compose 命令启动 

```yaml
version: '3'
services:
  api2d-proxy:
    image: easychen/api2d-proxy
    ports:
      - 127.0.0.1:9000:9000
```

```bash
docker-compose up -d
```

## 访问地址

访问地址为 `http://$ip:9000` ，不想用 9000 可以调整上述 docker 命令或者 docker-compose 。

## 腾讯云函数

> 腾讯云函数可以直接使用镜像，但蛋疼的是，它不支持远程镜像（比如docker hub的），因此只能自己构建并上传然后使用。

### 构建镜像并推送到腾讯云

参考 [腾讯云个人版镜像操作指南](https://cloud.tencent.com/document/product/1141/50310)

> api2d/proxy 部分请换成你自己的 命令空间/镜像名称

1. docker build -t api2d-proxy ./
1. docker tag api2d-proxy ccr.ccs.tencentyun.com/api2d/proxy:latest
1. docker push ccr.ccs.tencentyun.com/api2d/proxy:latest

### 创建

![](images/20230701224046.png)


### 获得访问地址

![](images/20230701224226.png)

### 在软件中填入

![](images/20230701224405.png)